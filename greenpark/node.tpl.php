﻿<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
	<?php if ($picture) { print $picture; } ?>
	<?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
	<div class="node_info">
		<?php print $submitted ?>
        <?php if ($terms) { ?><?php print $terms ?><?php } ?>
	</div>
	<div class="content">
		<?php if ($node_inner_right && $page !=0) { ?>
			<div id="node_inner_right"><?php print $node_inner_right?></div>
		<?php }; ?>
		<?php print $content?>
		<?php if ($node_inner_bottom && $page !=0) { ?>
			<div id="node_inner_bottom"><?php print $node_inner_bottom?></div>
		<?php }; ?>
	</div>
	<?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
</div>
