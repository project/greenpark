<?php
/**
	add regions to page.tpl.php
**/
function greenpark_regions() {
	return array(
		'left' => t('left sidebar'),
		'footer' => t('footer'),
		'content_top' => t('content_top'),
		'content_bottom' => t('content_bottom'),
		'node_inner_right' => t('node inner right'),
		'node_inner_bottom' => t('node inner bottom')
	);
} 
/**
	add regions to node.tpl.php
**/
function _phptemplate_variables($hook, $variables) {
	// loead the regions only for full views
	if($hook == 'node' && !$vars['teaser']) {
		//load region content assigned via blocks
			foreach(array('node_inner_right') as $region) {
				$variables[$region] = theme('blocks', $region);
			}
			foreach(array('node_inner_bottom') as $region) {
				$variables[$region] = theme('blocks', $region);
			}
	}
	return $variables;
}
/**
* Implementation of theme_filter_tips_more_info().
* Used here to hide the "More information about formatting options" link.
*/
function phptemplate_filter_tips_more_info() {
	return '';
 } 